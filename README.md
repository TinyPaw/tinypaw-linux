+---------------------------------+

TinyPaw Linux

+---------------------------------+

v1.5 & v1.5_hybrid

+---------------------------------+

Release date: 09.15.20

+---------------------------------+

*Liability: the author and/or any contributors do not advocate the use on any network or AP other then your own. Thank you.

Built On:

-Core 9.0 / TinyPaw v1.4

+---------------------------------+

Inspired & Influenced By:

-Xiaopan OS & Xiaopan Community

+---------------------------------+

Inclusion of bully 1.4 - kimocoder fork

Inclusion of wifite2 2.5.2 - kimocoder fork 

Inclusion of medusa 2.2

Inclusion of geoip dat files

Inclusion of python 2.7 packages necessary for sslstrip

Persistent install backup data fix for shutdown / reboot - by default TinyPaw 
will only save runtime data from /opt , ~/Downloads and ~/hs directories. 

Minor fixes to the hackedbox menu / submenu including new "Quick Folders" 
section for quick navigation to various system and runtime program directories.

Package updates:

Aircrack-ng 1.6
    
Airgeddon 10.21
    
Bully 1.4 - fork
    
Ettercap 0.8.3.1
    
Fern-Wifi-Cracker 3.1
    
Hackedbox
    
MacChanger 1.7.0
    
MDK3 3.7 / 8
    
MDK4 4.1
    
NMap / ZenMap 7.80
    
pyPack_2.7 rev8
    
pyPack_3.6 rev4
    
PyQt5 5.15.10
    
PyQt5_sip 12.8.1
    
Reaver 1.6.6
    
Sip 5.4.0 
    
Wifite2 2.5.2 - fork
    
Wireshark 3.2.6
    
Xterm-customize

+---------------------------------+

End

+---------------------------------+


+---------------------------------+ 

TinyPaw Linux           
                                 
+---------------------------------+

v1.4 & v1.4_hybrid   

+---------------------------------+

Release date: 07.31.19       

+---------------------------------+

*Liability: the author and/or any 
contributors do not advocate the 
use on any network or AP other then 
your own. Thank you.

Built On:

-Core 9.0 / TinyPaw v1.3.1

+---------------------------------+

Inspired & Influenced By:

-Xiaopan OS & Xiaopan Community

+---------------------------------+

Changes from v1.3.1.1:

Inclusion of haschat processors

Inclusion of create_ap

Inclusion of dsniff

Inclusion of sslsplit

Inclusion of hiddeneye

Inclusion of zizzania

Inclusion of xterm runtime change options

Inclusion of default ~/.Xresources

Inclusion of netsurf browser

Inclusion of xf86-input-synaptics for additional input devices / touch input

Inclusion of asleap

Inclusion of hostapd-wpe

Inclusion of /home/tc/wordlists/ folder - contains easy access to wordlists used by fern , wifite , autopwner

Discontinue of fifth browser ( I'm sorry, it was terrible I know )

Discontinue of east framework

Removal of non-essential python2.7 modules

Package updates:

Airgeddon 9.20

Wireshark 3.0.2

Ettercap 0.8.3

Iptables 1.8.3

Wifi-autopwner 2019 build

Scapy 2.4.0

Fern-Wifi-Cracker 2.8

Xhydra 9.1

+---------------------------------+

End

+---------------------------------+


+---------------------------------+ 

TinyPaw Linux           
                                 
+---------------------------------+

v1.3.1.1 & v1.3.1.1_hybrid   

+---------------------------------+

Release date: 03.11.19       

+---------------------------------+

*Liability: the author and/or any 
contributors do not advocate the 
use on any network or AP other then 
your own. Thank you.

Built On:

-Core 9.0 / TinyPaw v1.3.1

+---------------------------------+

Inspired & Influenced By:

-Xiaopan OS & Xiaopan Community

+---------------------------------+

Changes from v1.3.1:

*Removal of RTL8812AU device driver as default package. Whether a systems compatibility
or one with the latest aircrack-ng release, this kernel driver seemed to be at the root of 
consistent / inconsistent spikes in CPU usage. 

RTL8812AU device driver will remain available from the project repositories. 

Identical to v1.3.1 in every way except for the above mentioned change.


+---------------------------------+

End

+---------------------------------+


+---------------------------------+ 

TinyPaw Linux           
                                 
+---------------------------------+

v1.3.1 & v1.3.1_hybrid   

+---------------------------------+

Release date: 02.10.19       

+---------------------------------+

*Liability: the author and/or any 
contributors do not advocate the 
use on any network or AP other then 
your own. Thank you.

Built On:

-Core 9.0 / TinyPaw v1.3

+---------------------------------+

Inspired & Influenced By:

-Xiaopan OS & Xiaopan Community

+---------------------------------+

Changes from v1.3:

Inclusion of haschat 32bit (non-OCL cpu only)

Inclusion of wifite script to enable PMKID attack without OCL-hashcat

Inclusion of kernel rtl8812au wifi device driver by default

Inclusion of CLI shortcut commands (airgeddon, autopwn, east, hashcat, revdk3, rsf, wifite)

Fixed wifi-autopwner syntax in master wifi-autopwner.sh

Fixed airdrop-ng & airgraph-ng python scripts

Package updates:

Aircrack-ng - 1.5.2

Airgeddon - 8.12*Inclusion of bully 1.4 - kimocoder fork

*Inclusion of wifite2 2.5.2 - kimocoder fork 

*Inclusion of medusa 2.2

*Inclusion of geoip dat files

*Inclusion of python 2.7 packages necessary for sslstrip

*Persistent install backup data fix for shutdown / reboot - by default TinyPaw 
will only save runtime data from /opt , ~/Downloads and ~/hs directories. 

*Minor fixes to the hackedbox menu / submenu including new "Quick Folders" 
section for quick navigation to various system and runtime program directories.



+---------------------------------+

End

+---------------------------------+


+---------------------------------+ 

TinyPaw Linux           
                                 
+---------------------------------+

v1.3 & v1.3_hybrid   

+---------------------------------+

Release date: 10.27.18       

+---------------------------------+

*Liability: the author and/or any 
contributors do not advocate the 
use on any network or AP other then 
your own. Thank you.

Built On:

-Core 9.0 / TinyPaw v1.2Z.1

+---------------------------------+

Inspired & Influenced By:

-Xiaopan OS & Xiaopan Community

+---------------------------------+

Changes from v1.2Z.1:

Discontinued Fluxion & Linset packages

Discontinued Seamonkey browser in favor of Fifth browser

Moved to Qt5 and PyQt5 and dropped non-essential Qt4 packages and PyQt4 entirely

Removed appsbrowser from wbar

Firmware, driver, wpa, core updates

Inclusion of Wifi-Autopwner2

Inclusion of JohnTheRipper

Package updates:

Aircrack-ng - 1.4

Airgeddon - 8.11

Ettercap - 0.8.2 - revision 2 w compiled filters

Fern-WiFi-Cracker - 2.7

Python3.6 modules

PyQt5 - 5.10.x

RouterSploit - 3.4.0

WiFite2 - 2.2.5

Wireshark - 2.6.4


+---------------------------------+

End

+---------------------------------+


+---------------------------------+ 

TinyPaw Linux           
                                 
+---------------------------------+

v1.2Z.1 & v1.2Z.1_hybrid              

+---------------------------------+

Release date: 7.21.18       

+---------------------------------+

*Liability: the author and/or any 
contributors do not advocate the 
use on any network or AP other then 
your own. Thank you.

Built On:

-Core 9.0 / TinyPaw v1.2Z

+---------------------------------+

Inspired & Influenced By:

-Xiaopan OS & Xiaopan Community

+---------------------------------+

Changes from v1.2Z:

Corrected functionality issue with newly compiled aircrack-ng 
extension affecting aireplay / deauth / etc causing the tool to hang & 
stall. Reverted to previous aircrack-ng 1.2 release build with 
experimental tool and extra scripts.

This was an update for core functionality and usage, everything 
else remains identical to v1.2Z

v1.2Z.1 release build was way before schedule and released only 
due to the nature of this affecting core functionality and 
performance. Thank you


+---------------------------------+

End

+---------------------------------+


+---------------------------------+ 

TinyPaw Linux           
                                 
+---------------------------------+

v1.2Z & v1.2Z_hybrid              

+---------------------------------+

Release date: 7.7.18       

+---------------------------------+

*Liability: the author and/or any 
contributors do not advocate the 
use on any network or AP other then 
your own. Thank you.

Built On:

-Core 9.0 / TinyPaw v1.2

+---------------------------------+

Inspired & Influenced By:

-Xiaopan OS & Xiaopan Community

+---------------------------------+

Changes from v1.2:

Corrected " command | tee output " issues with additional X11
libs and Xterm-333

Corrected Airgeddon dhcpd issues with Evil Twin by adjusting 
dhcpd.leases path in root .sh file

Compiled new libs:

libcares 1.14.0

libpcap 1.8.1

libnl 3.4.0

libgpg-error 1.31

libgcrypt 1.8.3

Compiled new build releases for previous tools:

Wireshark 2.6.1 QT

Reaver 1.6.5

SSLStrip2/SSLStrip+


+---------------------------------+

End

+---------------------------------+


+---------------------------------+ 

TinyPaw Linux           
                                 
+---------------------------------+

v1.2 & v1.2_hybrid              

+---------------------------------+

Release date: 5.23.18       

+---------------------------------+

*Liability: the author and/or any 
contributors do not advocate the 
use on any network or AP other then 
your own. Thank you.

Built On:

-Core 9.0 / TinyPaw v1.1.1

+---------------------------------+

Inspired & Influenced By:

-Xiaopan OS & Xiaopan Community

+---------------------------------+

Changes from v1.1.1:

Built on Tiny Core 9.0 - Kernel 4.14.10

Embedded attack scripts into the core (/home/) iso directory instead of using 
squashfs *.tcz extensions in order to overcome performance and r+rw issues.

Recompiled previous tools and extensions against new libraries, as well 
as adding gtk+ gui builds for previous tools: zenmap, hydra.

Reconfigured WiFite2 default "hs" directory to the home/tc/hs folder for easier 
access.

Reverted to Scapy 2.3.2 for Pyrit compatibility.

Included custom vesamenu boot and splash as well as motd/cli splash.

Custom hackedbox theming with menu/submenu for complete tool/script access. 

Compiled new build releases for previous tools:

Aircrack-ng 1.2 (c)

NMap/ZenMap 7.70svn

Hydra/XHydra 8.6 (decided against 8.7 until it's out of development class)

Wireshark 2.4.6

Mdk4

Crunch 3.6


+---------------------------------+

End

+---------------------------------+


+---------------------------------+ 

TinyPaw Linux           
                                 
+---------------------------------+

v1.1.1              

+---------------------------------+

Release date: 3:24:18       

+---------------------------------+

*Liability: the author and/or any 
contributors do not advocate the 
use on any network or AP other then 
your own. Thank you.

Built On:

-CorePlus 8.2.1 / TinyPaw v1.1

+---------------------------------+

Inspired & Influenced By:

-Xiaopan OS & Xiaopan Community

+---------------------------------+

Changes from v1.1:

Corrected pcregrep 8.41 & grep 3.1 accidental omission 
from LIVE and Install (onboot.lst) and (*base.lst)

This was an update for core functionality and usage, everything 
else remains identical to v1.1

v1.1.1 release build was way before schedule and released only 
due to the nature of this effecting core functionality and 
performance. Thank you


+---------------------------------+

End

+---------------------------------+


+---------------------------------+
                                 
TinyPaw Linux           
                                 
+---------------------------------+

v1.1               

+---------------------------------+

Release date: 2:4:18

+---------------------------------+

*Liability: the author and/or any 
contributors do not advocate the 
use on any network or AP other then 
your own. Thank you.

Built On:

-CorePlus 8.2.1

+---------------------------------+

Inspired & Influenced By:

-Xiaopan OS & Xiaopan Community

+---------------------------------+

Changes from v1.0:

Configured Boot menu with "Live" and "Installer" options

Removed excessive window managers from system

Recompiled tools for pcre 8.41 libraries

Cleaned up onboot.lst and removed non-essential extensions

Compiled and included python modules essential for:

Networking

System

Core

Utils

Cryptography

+---------------------------------+

Primary Tools:

CLI / Terminal

Aircrack-ng 1.2-rc4 suite

Cowpatty 4.6

Bully 1.1

Reaver 1.6.4

PixieWPS 1.4.2

Mdk3 v6

PenTBox 1.5

RouterSploit 2.2.1

Crunch 3.4

NMap 7.60SVN

Pyrit 0.5.1

WiFite v2-r87

WiFite2

Airgeddon 7.23

Fluxion 0.23

Linset 0.14

ReVdK3-r3

Hydra 5.9.1

SSLStrip 0.9

Macchanger 1.6.0

Scapy 2.4rc2

GUI

Fern-WiFi-Cracker 2.5

ZenMap 7.60SVN

EaST 2.0.0

Wireshark 2.2.1


+---------------------------------+

End               

+---------------------------------+


+---------------------------------+
                                
TinyPaw Linux           
                                 
+---------------------------------+

v1.0

+---------------------------------+

Release date: 12:29:17

+---------------------------------+

*Liability: the author and/or any 
contributors do not advocate the 
use on any network or AP other then 
your own. Thank you.

Built On:

-CorePlus 8.2.1

+---------------------------------+

Inspired & Influenced By:

-Xiaopan OS & Xiaopan Community

+---------------------------------+

Primary Tools:

CLI / Terminal

Aircrack-ng 1.2-rc4 suite

Cowpatty 4.6

Bully

Reaver 1.5.2

PixieWPS 1.4.1

Mdk3 v6

PenTBox 1.5

RouterSploit 2.2.1

Crunch

NMap

Pyrit 0.5.1

WiFite v2-r87

GUI

Wireshark 2.2.1

Added to the repo, but not included in v1.0 

EaST Framework 2.0.0

+---------------------------------+

End               

+---------------------------------+




